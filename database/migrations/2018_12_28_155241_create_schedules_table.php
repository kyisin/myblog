<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedules', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('flight_id')->unsigned();
            $table->date('date');
            $table->string('from');
            $table->string('to');
            $table->integer('terminal_id')->unsigned();
            $table->time('departure_time');
            $table->time('arrival_time');

            $table->foreign('flight_id')->references('id')->on('flights');
            $table->foreign('terminal_id')->references('id')->on('terminals');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedules');
    }
}
