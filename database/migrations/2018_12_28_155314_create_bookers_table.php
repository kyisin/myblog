<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('name');
            $table->string('gender');
            $table->string('type');
            $table->time('departure_time');
            $table->string('from');
            $table->string('to');
            $table->string('nrc');
            $table->string('phone_no');
            $table->string('name_issue');
            $table->integer('flight_id')->unsigned();
            $table->integer('company_id')->unsigned();
            $table->integer('seat')->unsigned();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('flight_id')->references('id')->on('flights');
            $table->foreign('company_id')->references('id')->on('company__infos');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookers');
    }
}
